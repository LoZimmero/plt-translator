package com.fioretti.gabriele.translator;

import java.util.regex.Pattern;

import com.fioretti.gabriele.translator.exceptions.PigLatinException;

public class PigLatinTranslator {
	
	protected String sentence;

	public PigLatinTranslator(String sentence) {
		this.sentence = sentence;
	}

	public String getSentence() {
		return sentence;
	}

	public String translate() throws PigLatinException {
		if (sentence == null) throw new PigLatinException();
		String result = "";
		if (sentence.equals("")) result = "nil";
		else if (Pattern.compile("^[aeiouy]\\w*y$", Pattern.CASE_INSENSITIVE).matcher(sentence).find()) {
			result = sentence + "nay";
		}
		return result;
	}
	
}
